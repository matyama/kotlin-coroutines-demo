#!/usr/bin/env bash

set -e

JAR_PATH=target/kotlin-coroutines-demo-1.0-SNAPSHOT-jar-with-dependencies.jar

java -jar -Dkotlinx.coroutines.debug ${JAR_PATH} $1
