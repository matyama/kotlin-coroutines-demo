package ai.blindspot.techtalk.kotlin.coroutines

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.CompletableDeferred
import kotlinx.coroutines.experimental.CoroutineName
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.NonCancellable
import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.cancelAndJoin
import kotlinx.coroutines.experimental.cancelChildren
import kotlinx.coroutines.experimental.channels.Channel
import kotlinx.coroutines.experimental.channels.ProducerJob
import kotlinx.coroutines.experimental.channels.ReceiveChannel
import kotlinx.coroutines.experimental.channels.SendChannel
import kotlinx.coroutines.experimental.channels.actor
import kotlinx.coroutines.experimental.channels.consumeEach
import kotlinx.coroutines.experimental.channels.produce
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.newSingleThreadContext
import kotlinx.coroutines.experimental.run
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.selects.select
import kotlinx.coroutines.experimental.sync.Mutex
import kotlinx.coroutines.experimental.sync.withLock
import kotlinx.coroutines.experimental.withTimeout
import kotlinx.coroutines.experimental.withTimeoutOrNull
import mu.KLogging
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.coroutines.experimental.CoroutineContext
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) = DemoRunner runDemo args[0]

object DemoRunner : KLogging() {

    private val demos = listOf<Demo>(
            Demo1, Demo2, Demo3, Demo4, Demo5, Demo6, Demo7, Demo8, Demo9, Demo10,
            Demo11, Demo12, Demo13, Demo14, Demo15, Demo16, Demo17, Demo18, Demo19, Demo20,
            Demo21, Demo22, Demo23, Demo24, Demo25, Demo26, Demo27, Demo28, Demo29, Demo30,
            Demo31, Demo32, Demo33, Demo34, Demo35, Demo36, Demo37, Demo38, Demo39, Demo40,
            Demo41, Demo42, Demo43, Demo44, Demo45, Demo46
    )
            .mapIndexed { i, demo -> (i + 1).toString() to demo }
            .associate { it }

    infix fun runDemo(demoId: String) = demos[demoId]
            ?.also { logger.info { "running demo $demoId" } }
            ?.runDemo()
            ?: logger.error { "execution of demo $demoId failed" }
}

interface Demo {
    fun runDemo()
}

/**
 * My first coroutine
 */
object Demo1 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        launch {
            delay(1000L)
            Demo1.logger.info { "coroutines!" }
        }
        Demo1.logger.info { "Hello " }
        delay(2000L) // non-blocking delay for 2 seconds to keep JVM alive
    }

}

/**
 * Coroutines' `launch` returns job instance
 */
object Demo2 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val job = launch {
            delay(1000L)
            Demo2.logger.info { "with job" }
        }
        Demo2.logger.info { "running " }
        job.join()
    }

}

/**
 * Extracting `suspend` function / method
 */
object Demo3 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val job = launch { doWorld() }
        Demo3.logger.info { "Hello," }
        job.join()
    }

    suspend fun doWorld() {
        delay(1000L)
        Demo3.logger.info { "World!" }
    }

}

/**
 * Try to do this with `Thread`s
 */
object Demo4 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val jobs = List(100_000) {
            launch {
                delay(1000L)
                print(".")
            }
        }
        jobs.forEach { it.join() }
        println()
        Demo4.logger.info { "Done!" }
    }

}

/**
 * Coroutines are like daemon threads
 */
object Demo5 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        launch {
            repeat(1000) { i ->
                Demo5.logger.info { "I'm sleeping $i ..." }
                delay(500L)
            }
        }
        delay(1300L)
    }
}

/**
 * Cancelling coroutine execution
 */
object Demo6 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val job = launch {
            repeat(1000) { i ->
                Demo6.logger.info { "I'm sleeping $i ..." }
                delay(500L)
            }
        }
        delay(1300L)
        Demo6.logger.info { "I'm tired of waiting!" }
        job.cancel()
        job.join()
        Demo6.logger.info { "Now I can quit." }
    }

}

/**
 * Cancellation is cooperative
 */
object Demo7 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val startTime = System.currentTimeMillis()
        val job = launch {
            var nextPrintTime = startTime
            var i = 0
            while (i < 5) {
                // print a message twice a second
                if (System.currentTimeMillis() >= nextPrintTime) {
                    logger.info { "I'm sleeping ${i++} ..." }
                    nextPrintTime += 500L
                }
            }
        }
        delay(1300L)
        logger.info { "I'm tired of waiting!" }
        job.cancelAndJoin()
        logger.info { "Now I can quit." }
    }

}

/**
 * Making computation code cancellable
 */
object Demo8 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val startTime = System.currentTimeMillis()
        val job = launch {
            var nextPrintTime = startTime
            var i = 0
            while (isActive) { // cancellable computation loop
                if (System.currentTimeMillis() >= nextPrintTime) {
                    logger.info { "I'm sleeping ${i++} ..." }
                    nextPrintTime += 500L
                }
            }
        }
        delay(1300L)
        logger.info { "I'm tired of waiting!" }
        job.cancelAndJoin()
        logger.info { "Now I can quit." }
    }

}

/**
 * Closing resources with finally
 */
object Demo9 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val job = launch {
            try {
                repeat(1000) { i ->
                    logger.info { "I'm sleeping $i ..." }
                    delay(500L)
                }
            } finally {
                logger.warn { "I'm running finally" }
            }
        }
        delay(1300L)
        logger.info { "I'm tired of waiting!" }
        job.cancelAndJoin()
        logger.info { "Now I can quit." }
    }

}

/**
 * Run non-cancellable block
 */
object Demo10 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val job = launch {
            try {
                repeat(1000) { i ->
                    logger.info { "I'm sleeping $i ..." }
                    delay(500L)
                }
            } finally {
                run(NonCancellable) {
                    logger.info { "I'm running finally" }
                    delay(1000L)
                    logger.info { "And I've just delayed for 1 sec because I'm non-cancellable" }
                }
            }
        }
        delay(1300L)
        logger.info { "I'm tired of waiting!" }
        job.cancelAndJoin()
        logger.info { "Now I can quit." }
    }
}

/**
 * Timeout
 */
object Demo11 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        withTimeout(1300L) {
            repeat(1000) { i ->
                logger.info { "I'm sleeping $i ..." }
                delay(500L)
            }
        }
    }

}

/**
 * Timeout or null
 */
object Demo12 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val result = withTimeoutOrNull(1300L) {
            repeat(1000) { i ->
                logger.info { "I'm sleeping $i ..." }
                delay(500L)
            }
            "Done" // will get cancelled before it produces this result
        }
        logger.info { "Result is $result" }
    }

}

suspend fun doSomethingUsefulOne(): Int {
    delay(1000L) // pretend we are doing something useful here
    return 13
}

suspend fun doSomethingUsefulTwo(): Int {
    delay(1000L) // pretend we are doing something useful here, too
    return 29
}

/**
 * Composing suspending functions: Sequential by default
 */
object Demo13 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val time = measureTimeMillis {
            val one = doSomethingUsefulOne()
            val two = doSomethingUsefulTwo()
            logger.info { "The answer is ${one + two}" }
        }
        logger.info { "Completed in $time ms" }
    }

}

/**
 * Concurrent using async
 */
object Demo14 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val time = measureTimeMillis {
            val one = async { doSomethingUsefulOne() }
            val two = async { doSomethingUsefulTwo() }
            val output = "The answer is ${one.await() + two.await()}"
            logger.info { output }
        }
        logger.info { "Completed in $time ms" }
    }

}

/**
 * Lazily started async
 */
object Demo15 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val time = measureTimeMillis {
            val one = async(start = CoroutineStart.LAZY) { doSomethingUsefulOne() }
            val two = async(start = CoroutineStart.LAZY) { doSomethingUsefulTwo() }
            val output = "The answer is ${one.await() + two.await()}"
            logger.info { output }
        }
        logger.info { "Completed in $time ms" }
    }

}

// The result type of asyncSomethingUsefulOne is Deferred<Int>
fun asyncSomethingUsefulOne() = async {
    doSomethingUsefulOne()
}

// The result type of asyncSomethingUsefulTwo is Deferred<Int>
fun asyncSomethingUsefulTwo() = async {
    doSomethingUsefulTwo()
}

/**
 * Async-style functions
 */
object Demo16 : Demo, KLogging() {

    override fun runDemo() {
        val time = measureTimeMillis {
            // we can initiate async actions outside of a coroutine
            val one = asyncSomethingUsefulOne()
            val two = asyncSomethingUsefulTwo()
            // but waiting for a result must involve either suspending or blocking.
            // here we use `runBlocking { ... }` to block the main thread while waiting for the result
            runBlocking {
                val output = "The answer is ${one.await() + two.await()}"
                logger.info { output }
            }
        }
        logger.info { "Completed in $time ms" }
    }

}

/**
 * Dispatchers and threads
 */
object Demo17 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val jobs = arrayListOf<Job>()
        jobs += launch(Unconfined) { // not confined -- will work with main thread
            logger.info { "Unconfined" }
        }
        jobs += launch(coroutineContext) { // context of the parent, runBlocking coroutine
            logger.info { "coroutineContext" }
        }
        jobs += launch(CommonPool) { // will get dispatched to ForkJoinPool.commonPool (or equivalent)
            logger.info { "CommonPool" }
        }
        jobs += launch(newSingleThreadContext("MyOwnThread")) { // will get its own new thread
            logger.info { "newSTC" }
        }
        jobs.forEach { it.join() }
    }

}

/**
 * Unconfined vs confined dispatcher
 */
object Demo18 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val jobs = arrayListOf<Job>()
        jobs += launch(Unconfined) { // not confined -- will work with main thread
            logger.info { "Unconfined" }
            delay(500)
            logger.info { "Unconfined: After delay" }
        }
        jobs += launch(coroutineContext) { // context of the parent, runBlocking coroutine
            logger.info { "coroutineContext" }
            delay(1000)
            logger.info { "coroutineContext: After delay" }
        }
        jobs.forEach { it.join() }
    }

}

/**
 * Debugging coroutines and threads: `-Dkotlinx.coroutines.debug`
 */
object Demo19 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val a = async(coroutineContext) {
            logger.info { "${Thread.currentThread().name}: I'm computing a piece of the answer" }
            6
        }
        val b = async(coroutineContext) {
            logger.info { "${Thread.currentThread().name}: I'm computing a piece of the answer" }
            7
        }
        val output = "${Thread.currentThread().name}: The answer is ${a.await() * b.await()}"
        logger.info { output }
    }

}

/**
 * Jumping between threads
 */
object Demo20 : Demo, KLogging() {

    override fun runDemo() = newSingleThreadContext("Ctx1").use { ctx1 ->
        newSingleThreadContext("Ctx2").use { ctx2 ->
            runBlocking(ctx1) {
                logger.info { "${Thread.currentThread().name}: Started in ctx1" }
                run(ctx2) {
                    logger.info { "${Thread.currentThread().name}: Working in ctx2" }
                }
                logger.info { "${Thread.currentThread().name}: Back to ctx1" }
            }
        }
    }

}

/**
 * Job in the context
 */
object Demo21 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        logger.info { "My job is ${coroutineContext[Job]}" }
    }

}

/**
 * Children of a coroutine
 */
object Demo22 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        // launch a coroutine to process some kind of incoming request
        val request = launch {
            // it spawns two other jobs, one with its separate context
            val job1 = launch {
                logger.info { "job1: I have my own context and execute independently!" }
                delay(1000)
                logger.info { "job1: I am not affected by cancellation of the request" }
            }
            // and the other inherits the parent context
            val job2 = launch(coroutineContext) {
                logger.info { "job2: I am a child of the request coroutine" }
                delay(1000)
                logger.info { "job2: I will not execute this line if my parent request is cancelled" }
            }
            // request completes when both its sub-jobs complete:
            job1.join()
            job2.join()
        }
        delay(500)
        request.cancel() // cancel processing of the request
        delay(1000) // delay a second to see what happens
        logger.info { "Who has survived request cancellation?" }
    }

}

/**
 * Combining contexts
 */
object Demo23 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        // start a coroutine to process some kind of incoming request
        val request = launch(coroutineContext) { // use the context of `runBlocking`
            // spawns CPU-intensive child job in CommonPool !!!
            val job = launch(coroutineContext + CommonPool) {
                logger.info { "job: I am a child of the request coroutine, but with a different dispatcher" }
                delay(1000)
                logger.info { "job: I will not execute this line if my parent request is cancelled" }
            }
            job.join() // request completes when its sub-job completes
        }
        delay(500)
        request.cancel() // cancel processing of the request
        delay(1000) // delay a second to see what happens
        logger.info { "Who has survived request cancellation?" }
    }

}

/**
 * Parental responsibilities
 */
object Demo24 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        // launch a coroutine to process some kind of incoming request
        val request = launch {
            repeat(3) { i -> // launch a few children jobs
                launch(coroutineContext)  {
                    delay((i + 1) * 200L) // variable delay 200ms, 400ms, 600ms
                    logger.info { "Coroutine $i is done" }
                }
            }
            logger.info { "request: I'm done and I don't explicitly join my children that are still active" }
        }
        request.join() // wait for completion of the request, including all its children
        logger.info { "Now processing of the request is complete" }
    }

}

/**
 * Naming coroutines for debugging
 */
object Demo25 : Demo, KLogging() {

    override fun runDemo() = runBlocking(CoroutineName("main")) {
        logger.info { "Started main coroutine" }
        // run two background value computations
        val v1 = async(CoroutineName("v1coroutine")) {
            delay(500)
            logger.info { "Computing v1" }
            252
        }
        val v2 = async(CoroutineName("v2coroutine")) {
            delay(1000)
            logger.info { "Computing v2" }
            6
        }
        val output = "The answer for v1 / v2 = ${v1.await() / v2.await()}"
        logger.info { output }
    }

}

/**
 * Cancellation via explicit job
 */
object Demo26 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val job = Job() // create a job object to manage our lifecycle
        // now launch ten coroutines for a demo, each working for a different time
        val coroutines = List(10) { i ->
            // they are all children of our job object
            launch(coroutineContext + job) { // we use the context of main runBlocking thread, but with our own job object
                delay((i + 1) * 200L) // variable delay 200ms, 400ms, ... etc
                logger.info { "Coroutine $i is done" }
            }
        }
        logger.info { "Launched ${coroutines.size} coroutines" }
        delay(500L) // delay for half a second
        logger.info { "Cancelling the job!" }
        job.cancelAndJoin() // cancel all our coroutines and wait for all of them to complete
    }

}

/**
 * Channels: Channel basics
 */
object Demo27 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val channel = Channel<Int>()
        launch {
            // this might be heavy CPU-consuming computation or async logic, we'll just send five squares
            (1..5)
                    .map { x -> x * x }
                    .forEach { channel.send(it) }
        }
        // here we print five received integers:
        repeat(5) {
            val receivedValue = channel.receive()
            logger.info { receivedValue }
        }
        logger.info { "Done!" }
    }

}

/**
 * Closing and iteration over channels
 */
object Demo28 : Demo, KLogging() {

    override fun runDemo() = runBlocking {
        val channel = Channel<Int>()
        launch {
            (1..5)
                    .map { x -> x * x }
                    .forEach { channel.send(it) }
                    .also { channel.close() } // we're done sending
        }
        // here we print received values using `for` loop (until the channel is closed)
        for (y in channel) {
            logger.info { y }
        }
        logger.info { "Done!" }
    }

}

/**
 * Building channel producers
 */
object Demo29 : Demo, KLogging() {

    private fun produceSquares() = produce {
        (1..5)
                .map { x -> x * x }
                .forEach { send(it) }
    }

    override fun runDemo() = runBlocking {
        val squares = produceSquares()
        squares.consumeEach { logger.info { it } }
        logger.info { "Done!" }
    }

}

/**
 * Pipelines
 */
object Demo30 : Demo, KLogging() {

    private fun produceNumbers() = produce {
        var x = 1
        while (true) send(x++) // infinite stream of integers starting from 1
    }

    private fun square(numbers: ReceiveChannel<Int>) = produce {
        for (x in numbers) send(x * x)
    }

    override fun runDemo() = runBlocking<Unit> {
        val numbers = produceNumbers() // produces integers from 1 and on
        val squares = square(numbers) // squares integers
        for (i in 1..5) println(squares.receive()) // print first five
        logger.info { "Done!" }
        squares.cancel() // need to cancel these coroutines in a larger app
        numbers.cancel()
    }

}

/**
 * Prime numbers with pipeline
 */
object Demo31 : Demo, KLogging() {

    private fun numbersFrom(context: CoroutineContext, start: Int) = produce(context) {
        var x = start
        while (true) send(x++) // infinite stream of integers from start
    }

    private fun filter(context: CoroutineContext, numbers: ReceiveChannel<Int>, prime: Int) = produce(context) {
        for (x in numbers)
            if (x % prime != 0)
                send(x)
    }

    private suspend tailrec fun printNextPrime(context: CoroutineContext, cur: ProducerJob<Int>, i: Int, n: Int) {
        if (i <= n) {
            val prime = cur.receive()
            logger.info { prime }
            printNextPrime(context = context, cur = filter(context, cur, prime), i = i + 1, n = n)
        }
    }

    override fun runDemo() = runBlocking {
        val cur = numbersFrom(coroutineContext, 2)
        printNextPrime(coroutineContext, cur, 1, 20)
        coroutineContext.cancelChildren() // cancel all children to let main finish
    }

}

/**
 * Fan-out
 */
object Demo32 : Demo, KLogging() {

    private fun produceNumbers() = produce {
        var x = 1 // start from 1
        while (true) {
            send(x++) // produce next
            delay(100) // wait 0.1s
        }
    }

    private fun launchProcessor(id: Int, channel: ReceiveChannel<Int>) = launch {
        channel.consumeEach {
            logger.info { "Processor #$id received $it" }
        }
    }

    override fun runDemo() = runBlocking<Unit> {
        val producer = produceNumbers()
        repeat(5) { launchProcessor(it, producer) }
        delay(950)
        producer.cancel() // cancel producer coroutine and thus kill them all
    }

}

/**
 * Fan-in
 */
object Demo33 : Demo, KLogging() {

    private suspend fun sendString(channel: SendChannel<String>, s: String, time: Long) {
        while (true) {
            delay(time)
            channel.send(s)
        }
    }

    override fun runDemo() = runBlocking {
        val channel = Channel<String>()
        launch(coroutineContext) { sendString(channel, "foo", 200L) }
        launch(coroutineContext) { sendString(channel, "BAR!", 500L) }
        repeat(6) { // receive first six
            val receivedString = channel.receive()
            logger.info { receivedString }
        }
        coroutineContext.cancelChildren() // cancel all children to let main finish
    }

}

/**
 * Buffered channels
 */
object Demo34 : Demo, KLogging() {

    override fun runDemo() = runBlocking<Unit> {
        val channel = Channel<Int>(4) // create buffered channel
        val sender = launch(coroutineContext) { // launch sender coroutine
            repeat(10) {
                logger.info { "Sending $it" } // print before sending each element
                channel.send(it) // will suspend when buffer is full
            }
        }
        // don't receive anything... just wait....
        delay(1000)
        sender.cancel() // cancel sender coroutine
    }

}

/**
 * Channels are fair
 */
object Demo35 : Demo, KLogging() {

    private data class Ball(val hits: Int)

    private fun Ball.hit(): Ball = copy(hits = hits + 1)

    private suspend fun player(name: String, table: Channel<Ball>) {
        for (ball in table) { // receive the ball in a loop
            val returnedBall = ball.hit()
            logger.info { "$name $returnedBall" }
            delay(300) // wait a bit
            table.send(returnedBall) // send the ball back
        }
    }

    override fun runDemo() = runBlocking {
        val table = Channel<Ball>() // a shared table
        launch(coroutineContext) { player("ping", table) }
        launch(coroutineContext) { player("pong", table) }
        val servedBall = Ball(hits = 0)
        table.send(servedBall) // serve the ball
        delay(10000) // delay 10 seconds
        coroutineContext.cancelChildren() // game over, cancel them
    }

}

suspend fun massiveRun(context: CoroutineContext, action: suspend () -> Unit) {
    val n = 1000 // number of coroutines to launch
    val k = 1000 // times an action is repeated by each coroutine
    val time = measureTimeMillis {
        val jobs = List(n) {
            launch(context) {
                repeat(k) { action() }
            }
        }
        jobs.forEach { it.join() }
    }
    println("Completed ${n * k} actions in $time ms")
}

/**
 * Shared mutable state and concurrency: The problem
 */
object Demo36 : Demo, KLogging() {

    @Volatile // in Kotlin `volatile` is an annotation -> but it does not help (`counter++` is read-update-write)
    private var counter = 0

    override fun runDemo() = runBlocking {
        massiveRun(CommonPool) {
            counter++
        }
        logger.info { "Counter = $counter" }
    }

}

/**
 * Thread-safe data structures
 */
object Demo37 : Demo, KLogging() {

    private val counter = AtomicInteger()

    override fun runDemo() = runBlocking {
        massiveRun(CommonPool) {
            counter.incrementAndGet()
        }
        logger.info { "Counter = ${counter.get()}" }
    }

}

/**
 * Thread confinement fine-grained
 */
object Demo38 : Demo, KLogging() {

    private val counterContext = newSingleThreadContext("CounterContext")
    private var counter = 0

    override fun runDemo() = runBlocking {
        massiveRun(CommonPool) { // run each coroutine in CommonPool
            run(counterContext) { // but confine each increment to the single-threaded context
                // Each individual increment switches from multi-threaded `CommonPool` context to the
                // single-threaded context using `run` block
                counter++
            }
        }
        logger.info { "Counter = $counter" }
    }

}

/**
 * Thread confinement coarse-grained
 */
object Demo39 : Demo, KLogging() {

    private val counterContext = newSingleThreadContext("CounterContext")
    private var counter = 0

    override fun runDemo() = runBlocking {
        massiveRun(counterContext) { // run each coroutine in the single-threaded context
            counter++
        }
        logger.info { "Counter = $counter" }
    }

}

/**
 * Mutual exclusion
 */
object Demo40 : Demo, KLogging() {

    private val mutex = Mutex()
    private var counter = 0

    override fun runDemo() = runBlocking {
        massiveRun(CommonPool) {
            mutex.withLock { // but it's fine-grained :(
                counter++
            }
        }
        logger.info { "Counter = $counter" }
    }

}

// Message types for counterActor
sealed class CounterMsg
object IncrementCounter : CounterMsg() // one-way message to increment counter
class GetCounter(val response: CompletableDeferred<Int>) : CounterMsg() // a request with reply

/**
 * Actors
 */
object Demo41 : Demo, KLogging() {

    // This function launches a new counter actor
    private fun counterActor() = actor<CounterMsg> {
        var counter = 0 // actor state
        // iterate over incoming
        channel.consumeEach { msg ->
            when (msg) {
                is IncrementCounter -> counter++
                is GetCounter -> msg.response.complete(counter)
            }
        }
    }

    override fun runDemo() = runBlocking<Unit> {
        val counter = counterActor() // create the actor
        massiveRun(CommonPool) {
            counter.send(IncrementCounter)
        }
        // send a message to get a counter value from an actor
        CompletableDeferred<Int>()
                .also { response -> counter.send(GetCounter(response)) }
                .await()
                .also { logger.info { "Counter = $it" } }
        counter.close() // shutdown the actor
    }

}

/**
 * Selecting from channels
 */
object Demo42 : Demo, KLogging() {

    private fun fizz(context: CoroutineContext) = produce(context) {
        while (true) { // sends "Fizz" every 300 ms
            delay(300)
            send("Fizz")
        }
    }

    private fun buzz(context: CoroutineContext) = produce(context) {
        while (true) { // sends "Buzz!" every 500 ms
            delay(500)
            send("Buzz!")
        }
    }

    suspend fun selectFizzBuzz(fizz: ReceiveChannel<String>, buzz: ReceiveChannel<String>) =
            select<Unit> { // <Unit> means that this select expression does not produce any result
                fizz.onReceive { value ->  // this is the first select clause
                    logger.info { "fizz -> '$value'" }
                }
                buzz.onReceive { value ->  // this is the second select clause
                    logger.info { "buzz -> '$value'" }
                }
            }

    override fun runDemo() = runBlocking {
        val fizz = fizz(coroutineContext)
        val buzz = buzz(coroutineContext)
        repeat(7) {
            selectFizzBuzz(fizz, buzz)
        }
        coroutineContext.cancelChildren() // cancel fizz & buzz coroutines
    }

}

/**
 * Selecting on close
 */
object Demo43 : Demo, KLogging() {

    private suspend fun selectAorB(a: ReceiveChannel<String>, b: ReceiveChannel<String>): String = select {
        a.onReceiveOrNull { value ->
            value?.let { "a -> '$it'" } ?: "Channel 'a' is closed"
        }
        b.onReceiveOrNull { value ->
            value?.let { "b -> '$it'" } ?: "Channel 'b' is closed"
        }
    }

    override fun runDemo() = runBlocking {
        // we are using the context of the main thread in this example for predictability ...
        val a = produce(coroutineContext) { // select is biased to the first matching clause
            repeat(4) { send("Hello $it") }
        }
        val b = produce(coroutineContext) {
            repeat(4) { send("World $it") }
        }
        repeat(8) { // print first eight results
            val selectedOutput = selectAorB(a, b)
            logger.info { selectedOutput }
        }
        coroutineContext.cancelChildren()
    }

}

/**
 * Selecting to send
 */
object Demo44 : Demo, KLogging() {

    private fun produceNumbers(context: CoroutineContext, side: SendChannel<Int>) = produce<Int>(context) {
        for (num in 1..10) { // produce 10 numbers from 1 to 10
            delay(100) // every 100 ms
            select<Unit> {
                onSend(num) {} // Send to the primary channel
                side.onSend(num) {} // or to the side channel
            }
        }
    }

    override fun runDemo() = runBlocking<Unit> {
        val side = Channel<Int>() // allocate side channel
        launch(coroutineContext) { // this is a very fast consumer for the side channel
            side.consumeEach { logger.info { "Side channel has $it" } }
        }
        produceNumbers(coroutineContext, side).consumeEach {
            logger.info { "Consuming $it" }
            delay(250) // let us digest the consumed number properly, do not hurry
        }
        logger.info { "Done consuming" }
        coroutineContext.cancelChildren()
    }

}

/**
 * Selecting deferred values
 */
object Demo45 : Demo, KLogging() {

    private fun asyncString(time: Int): Deferred<String> = async {
        delay(time.toLong())
        "Waited for $time ms"
    }

    private fun asyncStringsList(): List<Deferred<String>> {
        val random = Random(3)
        return List(12) { asyncString(random.nextInt(1000)) }
    }

    override fun runDemo() = runBlocking {
        val list = asyncStringsList()
        val result = select<String> {
            list.forEachIndexed { index, deferred ->
                deferred.onAwait { answer ->
                    "Deferred $index produced answer '$answer'"
                }
            }
        }
        logger.info { result }
        val countActive = list.count { it.isActive }
        logger.info { "$countActive coroutines are still active" }
    }

}

/**
 * Switch over a channel of deferred values
 */
object Demo46 : Demo, KLogging() {

    private fun switchMapDeferreds(input: ReceiveChannel<Deferred<String>>): ProducerJob<String> = produce {
        var current = input.receive() // start with first received deferred value
        while (isActive) { // loop while not cancelled/closed
            val next = select<Deferred<String>?> { // return next deferred value from this select or null
                input.onReceiveOrNull { update ->
                    update // replaces next value to wait
                }
                current.onAwait { value ->
                    send(value) // send value that current deferred has produced
                    input.receiveOrNull() // and use the next deferred from the input channel
                }
            }
            if (next == null) {
                logger.info { "Channel was closed" }
                break // out of loop
            } else {
                current = next
            }
        }
    }

    private fun asyncString(str: String, time: Long): Deferred<String> = async {
        delay(time)
        str
    }

    override fun runDemo() = runBlocking {
        val chan = Channel<Deferred<String>>() // the channel for test
        launch(coroutineContext) { // launch printing coroutine
            switchMapDeferreds(chan).consumeEach { s ->
                logger.info { s } // print each received string
            }
        }
        chan.send(asyncString("BEGIN", 100))
        delay(200) // enough time for "BEGIN" to be produced
        chan.send(asyncString("Slow", 500))
        delay(100) // not enough time to produce slow
        chan.send(asyncString("Replace", 100))
        delay(500) // give it time before the last one
        chan.send(asyncString("END", 500))
        delay(1000) // give it time to process
        chan.close() // close the channel ...
        delay(500) // and wait some time to let it finish
    }

}
